import os.path

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_DIR = os.path.join(ROOT_DIR, 'config')
LOG_DIR = os.path.join(ROOT_DIR, 'log')
LOG_PATH = os.path.join(LOG_DIR, 'log.log')
USERS_PATH = os.path.join(CONFIG_DIR, 'users.json')
CONFIG_PATH = os.path.join(CONFIG_DIR, 'config.toml')
TIMETABLE_PATH = os.path.join(CONFIG_DIR, 'timetable.xlsx')

TELEGRAM_REQUEST_URL = 'https://api.telegram.org/bot{token}/{method}'

WEBHOOK_URL = 'https://{address}:443/{token}'

SSL_DIR = os.path.join(ROOT_DIR, 'ssl')
SSL_KEY = os.path.join(SSL_DIR, 'privkey.pem')
SSL_CERTIFICATE = os.path.join(SSL_DIR, 'fullchain.pem')

ERROR_NOT_A_COMMAND = '"{0}" не является командой! Используйте /info для помощи.'
ERROR_WRONG_COMMAND = '"{0}" нет в списке известных команд, либо я не могу ее распонать! Используйте /info для помощи.'
ERROR_NONE_GROUP = 'Ошибка! Перед использованием данных команд требуется выбрать группу!'
ERROR_WRONG_GROUP = 'Ошибка! Неправильно указана группа.'
ERROR_WRONG_DATE = 'Ошибка! Неправильно введена дата.'

MESSAGE_START = 'Привет, приятель! Выбери свою группу, дабы я, великий Бот, смог выводить расписание!'
MESSAGE_QUESTION = 'Что я должен сделать?'
MESSAGE_GROUP = 'Выберите группу из предложенных'
MESSAGE_INFO = '*[GitLab](https://gitlab.com/MaxZ043/telegram-timetable-bot)* \- репозиторий \n' \
               '*[VK](https://vk.com/maxzvagin)* \- страничка автора \n' \
               '\n*Команды:* \n' \
               '*/info* \- список команд, информация о боте \(рекурсия\) \n' \
               '*/groups* \- список поддерживаемых групп \n' \
               '\n*Расписание:* \n' \
               '*сегодня* \- расписание на текущий день \n' \
               '*завтра* \- расписание на следующий день \n' \
               '\n*\[дата\]* \- расписание на конкретный день, вводить в формате \[ДД\], \[ДД\.ММ\], \[ДД\.ММ\.ГГ\]\.' \
               '\n' \
               '*Пример:* 29, 29\.9, 29\.9\.2022 \n' \
               '\n*\[группа\]* \- смена группы\. \n' \
               '*Пример:* МВС\-121'
MESSAGE_NO_SUBJECTS = 'Поздравляю, пар нет и вы можете расслабиться!'
MESSAGE_GROUP_UPDATED = 'Поздравляю, группа успешно обновлена на {0}!'
