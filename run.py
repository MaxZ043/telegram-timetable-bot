from time import sleep
from flask import Flask, request
from waitress import serve
import module.configParser as cfg
import module.commandHandler as comhand
import module.responseHandler as resphand
import module.requestHandler as reqHandler

app = Flask(__name__)


@app.route('/' + cfg.options_list['token'], methods=['GET', 'POST'])
def receive_update():
    if request.method == 'POST':
        chat_id = int(request.json['message']['chat']['id'])
        user_id = int(request.json['message']['from']['id'])
        message_txt = request.json['message']['text']
        response = resphand.assemble_response(comhand.handle_incoming_message(message_txt, user_id), chat_id)
        reqHandler.send_message(response)
    return {"ok": True}


if __name__ == '__main__':
    reqHandler.remove_webhook()
    sleep(1)
    reqHandler.set_webhook()
    # import definitions as defines
    # app.run(host='0.0.0.0', port=cfg.options_list['port'], ssl_context=(defines.SSL_CERTIFICATE, defines.SSL_KEY))
    serve(app, host='0.0.0.0', port=cfg.options_list['port'], url_scheme='https')
