# Telegram Timetable Bot
Простой бот, который создан для обработки таблиц с расписанием ВУЗа РГУ им. А.Н.Косыгина образца 2022 года.

В боте использованы библиотеки: tomli, pandas

## Как запустить?
Требуется положить таблицу с названием "timetable" в формате .xlsx в папку config, [создать Telegram бота](https://telegram.me/BotFather "Крёстнй бот-отец") через BotFather и указать его токен в config.toml

Далее открываем терминал, переходим в директорию с ботом и запускаем его командой:
```Terminal
python3 main.py &
```

Для остановки процесса используйте одну из команд ниже и найдите процесс main.py:
```Terminal
ps a - Linux Ubuntu
tasklist - Windows
```
Далее запомните PID процесса и введите в командную строку / терминал:
```Terminal
kill -9 [PID_NUMBER] - Linux Ubuntu
taskkill /F /PID [PID_NUMBER] - Windows
```

## Об авторе
Молодой говнокодер из Москвы, найти можно [по ссылке](https://vk.com/maxzvagin "ВКшечка").
