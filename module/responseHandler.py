import module.excelParser as timetable


def assemble_groups_buttons():
    button_list = list()
    for i in timetable.excel_table_sheets:
        button_list.append([{'text': i.upper()}])
    button_list.append([{'text': 'Назад'}])
    reply_markup = {
        'keyboard':
            button_list,
        'resize_keyboard':
            True,
        'one_time_keyboard':
            False,
        'input_field_placeholder':
            'Группы'
    }
    return reply_markup


def assemble_menu_buttons():
    button_list = [[{'text': 'Сегодня'}], [{'text': 'Завтра'}], [{'text': 'Группа'}]]
    reply_markup = {
        'keyboard':
            button_list,
        'one_time_keyboard':
            False,
        'input_field_placeholder':
            'Меню'
    }
    return reply_markup


menu_dict = {'groups': assemble_groups_buttons, 'menu': assemble_menu_buttons}


def assemble_response(handled_command, chat_id: int):
    parse_mode = ''
    if type(handled_command) == dict:
        message = handled_command['message']
        try:
            parse_mode = handled_command['parse_mode']
        except KeyError:
            parse_mode = ''
        reply_markup = menu_dict[handled_command['type']]()
    else:
        message = handled_command
        reply_markup = menu_dict['menu']()
    response_json = {
        'chat_id':
            chat_id,
        'text':
            message,
        'parse_mode':
            parse_mode,
        'reply_markup':
            reply_markup
    }
    return response_json
