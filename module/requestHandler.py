import requests
import definitions as defines
import module.configParser as cfg

bot_token = cfg.options_list['token']
bot_url = defines.TELEGRAM_REQUEST_URL


def send_message(data):
    url = bot_url.format(token=bot_token, method='sendMessage')
    requests.post(url, json=data)


def set_webhook():
    url = bot_url.format(token=bot_token, method='setWebhook')
    webhook_url = defines.WEBHOOK_URL.format(address=cfg.options_list['address'], token=bot_token)
    requests.post(url, data={'url': webhook_url})


def remove_webhook():
    url = bot_url.format(token=bot_token, method='deleteWebhook')
    requests.post(url, data={'drop_pending_updates': True})
