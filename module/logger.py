import logging
from datetime import datetime
import definitions as defines
import module.configParser as cfg
import logging.handlers as handlers


def setup_logger():
    logging._defaultFormatter = logging.Formatter(u"%(message)s")
    logger = logging.getLogger('py_telegram_timetable_bot')
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    log_handler = handlers.TimedRotatingFileHandler(defines.LOG_PATH, when='W0')
    log_handler.setFormatter(formatter)

    logger.addHandler(log_handler)

    return logger


print_out = cfg.options_list['print_to_term']
app_logger = setup_logger()


def print_info(log_msg: str):
    print('[<<<] Log printed on {0}'.format(datetime.today()))
    print('\tLog info: {0}'.format(log_msg))


def log_info(log_msg: str):
    app_logger.info(log_msg)
    if print_out:
        print_info(log_msg)


def print_exception(log_msg: str):
    print('[<<<] Exception log printed on {0}'.format(datetime.today()))
    print('\tLog info: {0}'.format(log_msg))


def log_exception(log_msg: str):
    app_logger.exception(log_msg)
    if print_out:
        print_exception(log_msg)


def print_message(msg_id: str, msg_time: str, name: str, text: str):
    print('[<<<] Message received on {0}'.format(msg_time))
    print('\tText: {0}'.format(text))
    print('\tFrom: {0}'.format(name))
    print('\tMessage ID: {0}'.format(msg_id))


def log_message(message: dict):
    msg_time = datetime.fromtimestamp(message['message']['date']).strftime('%Y-%m-%d %H:%M:%S')
    msg_id = message['message']['message_id']
    msg_text = message['message']['text']
    user_name = message['message']['from'].get('first_name', '') + ' ' + message['message']['from'].get('last_name', '')
    msg_raw = 'Handled message [{msg_id}] from {user_name}: {msg_text}'.format(msg_id=msg_id, user_name=user_name,
                                                                               msg_text=msg_text)
    app_logger.info(msg_raw.encode('utf8', 'replace'))
    if print_out:
        print_message(msg_id, msg_time, user_name, msg_text)
