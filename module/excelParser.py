import pandas as pd
import module.logger as log
import module.configParser as cfg
import definitions as defines
import math

excel_table = pd.read_excel(defines.TIMETABLE_PATH, header=None, sheet_name=None, engine='openpyxl')
excel_table_sheets = list(excel_table.keys())


def is_value_nan(value):
    try:
        if type(value) == float and math.isnan(float(value)):
            return True
    except ValueError:
        return False


def are_strs_equal(str1: str, str2: str):
    try:
        if str1.casefold() == str2.casefold():
            return True
        else:
            return False
    except AttributeError:
        return False


def merge_time(subj_array: list, time: str):
    start_str = subj_array[len(subj_array) - 1]['time'].split('-')
    end_str = time.split('-')
    new_time = start_str[0] + '-' + end_str[1]
    subj_array[len(subj_array) - 1]['time'] = new_time
    return subj_array


def parse_timetable_by_sheet(key: int):
    return excel_table.get(excel_table_sheets[key])


def parse_timetable_to_list(excel_sheet: pd.DataFrame, has_pe=True):
    timetable_list: list = excel_sheet.values.tolist()
    parsed_dict = list()
    for i, row in enumerate(timetable_list):
        if is_value_nan(row[0]):
            continue
        if is_value_nan(row[4]) or are_strs_equal(row[4], cfg.options_list['pe_name']):
            if i >= 6 or not has_pe:
                continue
            index_max = min(len(timetable_list) - 1, i + 1)
            if i % 2 == 0 and (is_value_nan(timetable_list[index_max][4])
                               or are_strs_equal(row[4], timetable_list[index_max][4])):
                temp_dict = dict([('time', row[0]), ('loc', None), ('type', None),
                                  ('lector', None), ('subject', '[можно позаниматься физической культурой]\n')])
                parsed_dict.append(temp_dict)
            elif len(parsed_dict) >= 1:
                parsed_dict = merge_time(parsed_dict, row[0])
            continue
        if len(parsed_dict) >= 1 and are_strs_equal(row[4], parsed_dict[len(parsed_dict) - 1]['subject']):
            parsed_dict = merge_time(parsed_dict, row[0])
            continue
        temp_dict = dict([('time', row[0]), ('loc', row[1]), ('type', row[2]), ('lector', row[3]), ('subject', row[4])])
        parsed_dict.append(temp_dict)
    return parsed_dict


def parse_timetable_by_week(excel_sheet: pd.DataFrame):
    excel_sheet = excel_sheet.iloc[14:, ~excel_sheet.columns.isin([0, 11, 13, 14])].reset_index()
    week = list()
    end_row = 0
    for i in range(6):
        start_row = end_row + 1
        is_end = False
        for index, row in excel_sheet.iterrows():
            if type(row[1]) != int and index > start_row:
                end_row = int(index)
                is_end = True
                if i == 5:
                    break
                continue
            elif type(row[1]) == int and is_end:
                break
        even = excel_sheet.iloc[start_row:end_row, 7:14]
        even = even.iloc[:, ::-1]
        uneven = excel_sheet.iloc[start_row:end_row, 2:7]
        if i in cfg.options_list['day_wo_pe']:
            day = dict([('id', i), ('even', parse_timetable_to_list(even, False)),
                        ('uneven', parse_timetable_to_list(uneven, False))])
        else:
            day = dict(
                [('id', i), ('even', parse_timetable_to_list(even)), ('uneven', parse_timetable_to_list(uneven))])
        week.append(day)
    return week


def parse_timetable():
    parsed_timetable_sheets = list()
    for i in range(len(excel_table_sheets)):
        excel_sheet = parse_timetable_by_sheet(i)
        group = dict([('group_id', i), ('week', parse_timetable_by_week(excel_sheet))])
        parsed_timetable_sheets.append(group)
    log.log_info('Excel table parsed!')
    return parsed_timetable_sheets


by_group_list = parse_timetable()
