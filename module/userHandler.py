import json
import module.logger as log
import definitions as defines
from typing import Union, Dict


def create_user_dump(user_id: int, selected_group: id) -> Dict[str, Union[int, str]]:
    user: Dict[str, Union[str, int]] = {'id': user_id, 'pref_group': selected_group}
    return user


def update_users_json(users_list: list, user: dict = None):
    with open(defines.USERS_PATH, 'w') as f:
        if user:
            users_list.append(user)
            log.log_info( 'User entry %s created!' % user)
        json.dump(users_list, f, indent=2)


def open_or_create_users_json():
    try:
        open(defines.USERS_PATH)
    except FileNotFoundError:
        with open(defines.USERS_PATH, 'w') as users_file:
            json.dump(json.loads('[]'), users_file, indent=2)
    return json.load(open(defines.USERS_PATH))


def try_update_user_info(user_id: int, group_id: int = None):
    users_list = open_or_create_users_json()
    if len(users_list):
        for i in range(len(users_list)):
            if users_list[i]['id'] != user_id:
                continue
            users_list[i]['pref_group'] = group_id
            update_users_json(users_list)
            log.log_info('User %s info updated!' % user_id)
            return
    update_users_json(users_list, create_user_dump(user_id, group_id))


def try_load_user_info(user_id: int):
    users_json = open_or_create_users_json()
    if not len(users_json):
        try_update_user_info(user_id)
    group_id = None
    for i in range(len(users_json)):
        if users_json[i]['id'] != user_id:
            continue
        group_id = users_json[i]['pref_group']
        break
    return group_id
