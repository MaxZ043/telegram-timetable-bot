import datetime

import definitions as defines
import module.configParser as cfg
import module.userHandler as users
import module.excelParser as timetable


def handle_incoming_message(message_text: str, user_id: int):
    if message_text.startswith('/'):
        return handle_command(message_text)
    else:
        return handle_context_command(message_text, user_id)


def update_user_group(group: str, user_id: int):
    group_id = timetable.excel_table_sheets.index(group.lower())
    users.try_update_user_info(user_id, group_id)
    return defines.MESSAGE_GROUP_UPDATED.format(group)


def call_func_from_dict(func_name: str, func_dict: dict, user_id: int = None, message_text: str = None):
    if message_text:
        return func_dict[func_name](user_id, message_text)
    if user_id:
        return func_dict[func_name](user_id)
    return func_dict[func_name]()


def handle_context_command(message_text: str, user_id: int):
    if message_text.lower() in timetable.excel_table_sheets:
        return update_user_group(message_text, user_id)
    if message_text.lower() in context_commands:
        return call_func_from_dict(message_text.lower(), context_commands, user_id)
    if len(message_text.split('.')) > 0:
        return context_specific_date(message_text, user_id)
    return defines.ERROR_NOT_A_COMMAND.format(message_text)


def is_weekend(weekday_id: int):
    return weekday_id == 6 if True else False


def is_week_even(current: int):
    year_start = cfg.options_list['starting_date'].isocalendar()[1]
    return ((abs(year_start - current) - 1) % 2) == 0 if True else False


def build_message_from_dict(parsed_dict: dict, group_id: int, date: datetime.date):
    message = 'Расписание на {0} для группы {1} \n\n'.format(date, timetable.excel_table_sheets[group_id].upper())
    for i in range(len(parsed_dict)):
        # Let's check if our subject is not a placeholder
        if parsed_dict[i]['loc'] is not None:
            message += str(parsed_dict[i]['time']) + ' ' + parsed_dict[i]['subject'] + \
                       ', ' + str(parsed_dict[i]['loc']) + ', ' + parsed_dict[i]['lector'] + \
                       ', ' + parsed_dict[i]['type'] + '\n'
        else:
            message += str(parsed_dict[i]['time']) + ' ' + parsed_dict[i]['subject']
    return message


def parse_timetable_dict(group_id: int, date: datetime.date):
    week_id = date.isocalendar()[1]
    weekday_id = date.weekday()
    if is_weekend(weekday_id):
        return defines.MESSAGE_NO_SUBJECTS
    elif is_week_even(week_id):
        parsed_dict = timetable.by_group_list[group_id]['week'][weekday_id]['even']
    else:
        parsed_dict = timetable.by_group_list[group_id]['week'][weekday_id]['uneven']
    if len(parsed_dict) == 0:
        return defines.MESSAGE_NO_SUBJECTS
    return build_message_from_dict(parsed_dict, group_id, date)


def context_today(user_id: int):
    try:
        group_id = users.try_load_user_info(user_id)
        return parse_timetable_dict(group_id, datetime.date.today())
    except TypeError:
        return defines.ERROR_NONE_GROUP


def context_tomorrow(user_id: int):
    try:
        group_id = users.try_load_user_info(user_id)
        return parse_timetable_dict(group_id, datetime.date.today() + datetime.timedelta(days=1))
    except TypeError:
        return defines.ERROR_NONE_GROUP


def context_specific_date(message_text: str, user_id: int):
    try:
        group_id = users.try_load_user_info(user_id)
        temp = message_text.split('.')
        current_date = datetime.date.today()
        # We can receive date as DD.MM.YYYY, DD.MM, DD
        try:
            if len(temp) == 1:
                date = list([current_date.year, current_date.month, int(temp[0])])
            elif len(temp) == 2:
                date = list([current_date.year, int(temp[1]), int(temp[0])])
            else:
                date = list([int(temp[2]), int(temp[1]),  int(temp[0])])
            return parse_timetable_dict(group_id, datetime.date(date[0], date[1], date[2]))
        except (KeyError, TypeError, ValueError):
            return defines.ERROR_WRONG_DATE
    except TypeError:
        return defines.ERROR_NONE_GROUP


# noinspection PyUnusedLocal
def context_group(user_id: int):
    return dict([('type', 'groups'), ('message', defines.MESSAGE_GROUP)])


# noinspection PyUnusedLocal
def context_back(user_id: int):
    return dict([('type', 'menu'), ('message', defines.MESSAGE_QUESTION)])


def handle_command(message_text: str):
    if message_text.lower() not in simple_commands:
        return defines.ERROR_WRONG_COMMAND.format(message_text)
    return call_func_from_dict(message_text, simple_commands)


def command_start():
    return dict([('type', 'groups'), ('message', defines.MESSAGE_START)])


def command_info():
    return dict([('type', 'menu'), ('message', defines.MESSAGE_INFO), ('parse_mode', 'MarkdownV2')])


def command_groups():
    message = 'Доступные группы:'
    for i in timetable.excel_table_sheets:
        message += '\n - ' + i.upper()
    return message


context_commands = {'сегодня': context_today, 'завтра': context_tomorrow, 'группа': context_group,
                    'назад': context_back}
simple_commands = {'/start': command_start, '/info': command_info, '/groups': command_groups}
