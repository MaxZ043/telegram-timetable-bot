from typing import Dict

import definitions
import datetime
import tomli


def open_cfg_file() -> Dict:
    with open(definitions.CONFIG_PATH, 'rb') as f:
        cfg_file = tomli.load(f)
    return cfg_file


config = open_cfg_file()


def parse_config() -> Dict:
    return dict([('token', config['bot']['bot_token']), ('address', config['bot']['webhook_host']),
                 ('port', config['bot']['webhook_port']),
                 ('print_to_term', config['logging']['print_to_terminal']),
                 ('starting_date', config['parser']['study_starting_date']), ('pe_name', config['parser']['pe_name']),
                 ('day_wo_pe', config['parser']['day_wo_pe'])])


def validate_config(cfg: dict) -> bool:
    if type(cfg['token']) != str or type(cfg['address']) != str or type(cfg['port']) != str or type(
            cfg['print_to_term']) != bool or type(
            cfg['starting_date']) != datetime.date or type(cfg['day_wo_pe']) != list or type(cfg['pe_name']) != str:
        raise ValueError(f'invalid configuration: {config}')
    return True


def try_parse_n_validate_config() -> Dict:
    try:
        parsed_config = parse_config()
    except KeyError:
        raise ValueError(f'invalid configuration: {config}')
    if validate_config(parsed_config):
        return parsed_config


def load_config() -> Dict:
    return try_parse_n_validate_config()


options_list = load_config()
